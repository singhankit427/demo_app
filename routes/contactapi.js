const express=require('express');
const Contact=require('../model/Contact');

const route=express.Router();

route.post('/addContact',(req,res)=>{
    console.log(req.body);

    let{name,mobile,designation}=req.body;
    contact=new Contact();
    contact.name=name;
    contact.mobile=mobile;
    contact.designation=designation;
    
    contact.save((err,result)=>{
        if(err){
            return res.json(err)
        }
        else{
            return res.json("done")
        }
    })
})

route.get('/getContact',(req,res)=>{
    Contact.find({}).then(result=>{
        res.json(result);
    });
})
route.get('/getContact/:id',(req,res)=>{
    Contact.find({}).where({_id:req.params.id}).then(result=>{
        res.json(result);
    });
})

route.post('/updateContact/:id',(req,res)=>{
    console.log(req.params.id);
    let {name,mobile,designation}=req.body;
    Contact.updateOne({_id:req.params.id},{$set:{name:name,mobile:mobile,designation:designation}})
    .then(result=>{
        res.json(result)
    }).catch(err=>{
        res.json(err)
    })

})

route.post('/deleteContact/:id',(req,res)=>{
    console.log(req.params.id);
    // let {name,mobile,designation}=req.body;
    Contact.deleteOne({_id:req.params.id})
    .then(result=>{
        res.json(result)
    }).catch(err=>{
        res.json(err)
    })

})



module.exports=route;