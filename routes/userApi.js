const express=require('express');
const User=require('../model/User');


const route=express.Router();

route.post('/addUser',(req,res)=>{
    console.log(req.body)
    let{name,age,gender,email,contact_id}=req.body;
    user=new User({
        name:name,
        age:age,
        gender:gender,
        email:email,
        contact_id:contact_id
    })
    user.save((err,result)=>{
        if(err){
            res.json(err)
        }else{
            res.json(result)
        }
    })
})

route.get('/getUser',(req,res)=>{
    User.find({}).then(result=>{
        res.json(result)

    }).catch(err=>{
        res.json(err)
    })
    
})

route.get('/getUser/:id',(req,res)=>{
    User.find({}).where({_id:req.params.id}).then(result=>{
        res.json(result);
    });
})


route.post('/updateUser/:id',(req,res)=>{
    console.log(req.params.id);
    let{name,age,gender,email,contact_id}=req.body;
    User.updateOne({_id:req.params.id},{$set:{name:name,age:age,gender:gender,email:email,contact_id:contact_id}})
    .then(result=>{
        res.json(result)
    }).catch(err=>{
        res.json(err)
    })

})

route.post('/deleteUser/:id',(req,res)=>{
    console.log(req.params.id);
    // let {name,mobile,designation}=req.body;
    User.deleteOne({_id:req.params.id})
    .then(result=>{
        res.json(result)
    }).catch(err=>{
        res.json(err)
    })

})

module.exports=route;