const mongoose=require('mongoose');

const Schema=mongoose.Schema;

const userSchema=new Schema({
   name:{type:String,required:true},
   age:{type:Number,required:true},
   gender:{type:Boolean,require:true},
   email:{type:String,required:true},
   contact_id:{type:Schema.Types.ObjectId,ref:'contacts'}
});

module.exports=mongoose.model('userData',userSchema);
