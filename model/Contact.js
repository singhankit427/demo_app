const mongoose=require('mongoose');

const Schema=mongoose.Schema;

const contactSchema=new Schema({
   name:{type:String,required:true},
   mobile:{type:String,required:true},
   designation:{type:String,required:true}
   
});

module.exports=mongoose.model('contact',contactSchema);
