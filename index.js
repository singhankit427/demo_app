const express=require('express');

const bodyParser=require('body-parser');
const mongoose=require('mongoose');
const api=require('./routes/contactapi');
const userApi=require('./routes/userApi');

var url = "mongodb://localhost:27017/userDatabase"; 

mongoose.connect(url,{useNewUrlParser: true},(err,db)=>{
    if(err)
        console.log(err);
    else{
        console.log("Connect")
    }
        
})

const app=express();

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());
app.use('/apicontact',api);
app.use('/apiuser',userApi);


app.listen(3000,()=>{
    console.log("Done")
});